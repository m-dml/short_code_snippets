# short_code_snippets
A repo to keep track of and share single-file small code snippets.

All scripts in this repo are either stand-alone scripts or snippets containing
ideas and / or tutorials on how to do something.
If the script needs data for some example or for functioning, make sure that data is really
small (< 1mb!) and commit it in the data folder.