from tensorboard.backend.event_processing.event_accumulator import EventAccumulator

# an example tensorboard event file is in the data folder
tf_file = "data/events.out.tfevents.1642768526.g010.168432.6"

# set up what should be loaded.
# 0 means load all, 4 means load every 4th value
DEFAULT_SIZE_GUIDANCE = {
    "compressedHistograms": 1,
    "images": 0,
    "scalars": 0,
    "histograms": 1,
}

event_acc = EventAccumulator(tf_file, DEFAULT_SIZE_GUIDANCE)
event_acc.Reload()

# To see what is stored in the event file, have a look at the tags:
tags = event_acc.Tags()
print(tags)

# To get a dataframe of all scalars, you could for example do:
import pandas as pd

runlog_data = pd.DataFrame({"metric": [], "value": [], "step": []})
tags = event_acc.Tags()["scalars"]
for tag in tags:
    event_list = event_acc.Scalars(tag)
    values = list(map(lambda x: x.value, event_list))
    step = list(map(lambda x: x.step, event_list))
    r = {"metric": [tag] * len(step), "value": values, "step": step}
    r = pd.DataFrame(r)
    runlog_data = pd.concat([runlog_data, r])

print(runlog_data)