import matplotlib as mpl
from matplotlib.colors import ListedColormap
import seaborn as sns
import numpy as np
from cycler import cycler

hereon_color_array = np.array([
    [230, 0, 70],
    [0, 145, 160],
    [0, 170, 230],
    [250, 180, 35],
    [0, 70, 125],
    [175, 25, 60],
    [170, 200, 70],
    [250, 115, 80],
    [140, 90, 180],

])
hereon_color_array = hereon_color_array / 255
hereon_cmap = ListedColormap(hereon_color_array)
mpl.rc('image', cmap='gray')
mpl.rcParams['axes.prop_cycle'] = cycler(color=hereon_color_array)

# Example Matplotlib:
import matplotlib.pyplot as plt

data = np.random.uniform(1, 20, 5)

fig, ax = plt.subplots(figsize=(5, 5))
for i, bar in enumerate(data):
    ax.bar(i, bar)
plt.show()


# Example Seaborn:
import pandas as pd
sns.set()
sns.set_palette(hereon_color_array)

data_dict = {key: value for (key, value) in enumerate(data)}
data = pd.DataFrame.from_dict(data_dict, orient="index", columns=["value"]).reset_index()
sns.barplot(data=data, x="index", y="value")
plt.show()