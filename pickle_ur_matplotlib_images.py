import numpy as np
import matplotlib.pyplot as plt
import pickle as pl

# You can pickle your matplotlib image handles for
# furher manipulation, changing text size, data etc.

plt.rc("text", usetex=True)
plt.rc("font", family="serif")

def plot_pickle_sine(x, pickle_file_name, plot_file_name):
    fig_handle = plt.figure()
    y = np.sin(x)
    ax = fig_handle.add_subplot(3, 1, 1)
    (line,) = ax.plot(x, y)
    plt.draw()
    plt.title(r"$\sin(x)$")
    # Pickle the figure handle and save image
    f = open(pickle_file_name, "wb")
    plt.savefig(plot_file_name, dpi=500)
    pl.dump(fig_handle, f)


def plot_pickled_noisy(pickle_file_name, plot_file_name):
    noise = np.random.normal(0, 1, 100) * 0.1
    # Load the pickled handle for manipulation
    fig_handle = pl.load(open(pickle_file_name, "rb"))
    # Get the x axis data from the pickled handle
    x = fig_handle.gca().lines[0].get_xdata()
    y = np.sin(x + noise)
    ax = fig_handle.add_subplot(3, 1, 3)
    (line,) = ax.plot(x, y)
    plt.draw()
    plt.title(r"$\sin(x) +\epsilon$")
    plt.savefig(plot_file_name, dpi=500)

if __name__ == "__main__":
    x = np.linspace(0, 2 * np.pi, 100)
    pickle_file="sinus.pickle"
    plot_file="sine.jpg"
    plot_pickle_sine(x, pickle_file, plot_file)
    plot_file_noisy="sine_noise.jpg"
    plot_pickled_noisy(pickle_file, plot_file_noisy)