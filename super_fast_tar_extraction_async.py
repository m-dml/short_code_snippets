import asyncio
import glob
import os
import tarfile
from tqdm import tqdm


def extract(input_file, output_path, pbar=None):
    tar = tarfile.open(input_file)
    tar.extractall(path=output_path)
    tar.close()
    if pbar:
        pbar.update(1)


async def custom_system_command(command, pbar=None):
    command_list = command
    proc = await asyncio.create_subprocess_exec(*command_list)
    returncode = await proc.wait()
    if pbar:
        pbar.update(1)

    file = command[2]
    os.remove(file)

    return returncode


async def gather_with_concurrency(n, *tasks):
    semaphore = asyncio.Semaphore(n)

    async def sem_task(task):
        async with semaphore:
            return await task

    return await asyncio.gather(*(sem_task(task) for task in tasks))


async def prepare_system_command(commands):
    coroutines = [custom_system_command(command) for command in commands]
    await gather_with_concurrency(8, *coroutines)


def main(file: str, output_base_path: str):
    if not os.path.exists(output_base_path):
        os.makedirs(output_base_path)

    output_path = os.path.join(output_base_path, os.path.splitext(os.path.basename(file))[0])
    extract(file, output_path)
    print(f"Extracted base of {file}")
    files = glob.glob(os.path.join(output_path, "*"))

    commands = []
    for file in sorted(files):
        commands.append(["tar", "-xzf", file, "-C", output_path])

    loop = asyncio.get_event_loop()
    loop.run_until_complete(prepare_system_command(commands))


if __name__ == "__main__":
    OUTPUT_BASE_PATH = "/output"
    INPUT_DATA = "/tar_data/*.tar"

    files = glob.glob(INPUT_DATA)
    for file in tqdm(files):
        main(file, OUTPUT_BASE_PATH)