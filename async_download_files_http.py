"""
This script is for downloading data in an asynchronous way (recommended if you have to download a lot of
files).
Usage: define the addresses/files to download in the protected __main__ part so that `url_list` contains
        only complete http-paths to existing files.
Example:
    ```python
    OUTPUT_PATH = "."
    url_list = ["https://opendata.dwd.de/climate_environment/CDC/grids_germany/5_minutes/radolan/reproc/2017_002/bin/" \
               "2002/YW2017.002_200201.tar",
               "https://opendata.dwd.de/climate_environment/CDC/grids_germany/5_minutes/radolan/reproc/2017_002/bin/" \
               "2002/YW2017.002_200202.tar"]
    main(urls=url_list)
    ```
"""

import asyncio
import os

import aiohttp
import numpy as np


async def download_and_save_file(session, url, chunk_size=10, overwrite=True):
    # print("downloading: ", url)
    async with session.get(url) as response:
        assert response.status == 200
        file_name = os.path.split(url)[-1]
        if not os.path.isfile(os.path.join(OUTPUT_PATH, file_name)) or overwrite:
            with open(os.path.join(OUTPUT_PATH, file_name), 'wb') as fd:
                while True:
                    chunk = await response.content.read(chunk_size)
                    if not chunk:
                        break
                    fd.write(chunk)
            print("finished: ", file_name)
        else:
            print(f"{file_name} already exists and therefore is skipped")


async def gather_with_concurrency(n, *tasks):
    semaphore = asyncio.Semaphore(n)

    async def sem_task(task):
        async with semaphore:
            return await task
    return await asyncio.gather(*(sem_task(task) for task in tasks))


async def download_multiple(urls: list):
    timeout = aiohttp.ClientTimeout(total=6000)
    async with aiohttp.ClientSession(timeout=timeout) as session:
        coroutines = [download_and_save_file(session=session, url=url) for url in urls]
        await gather_with_concurrency(5, *coroutines)


def main(urls):
    loop = asyncio.get_event_loop()
    loop.run_until_complete(download_multiple(urls))
    print('finished')


if __name__ == "__main__":
    # # to download radar data:
    # OUTPUT_PATH = "/gpfs/work/machnitz/radolan/"
    # BASE_URL = "https://opendata.dwd.de/climate_environment/CDC/grids_germany/5_minutes/radolan/reproc/2017_002/bin/" \
    #            "{year}/YW2017.002_{year}{month}.tar"

    # # to download horizontal U wind:
    OUTPUT_PATH = "/gpfs/work/machnitz/cosmo/tar_data/2D/U_10M"
    BASE_URL = "https://opendata.dwd.de/climate_environment/REA/COSMO_REA6/hourly/2D/U_10M/" \
               "U_10M.2D.{year}{month}.grb.bz2"

    # # to download horizontal V wind:
    # OUTPUT_PATH = "/gpfs/work/machnitz/cosmo/tar_data/2D/V_10M"
    # BASE_URL = "https://opendata.dwd.de/climate_environment/REA/COSMO_REA6/hourly/2D/V_10M/" \
    #            "V_10M.2D.{year}{month}.grb.bz2"

    url_list = []
    for year in np.arange(2001, 2020, 1):
        for month in np.arange(1, 13, 1):
            month_str = f"{month:02d}"
            url_list.append(BASE_URL.format(year=str(year), month=str(month_str)))

    main(urls=url_list)